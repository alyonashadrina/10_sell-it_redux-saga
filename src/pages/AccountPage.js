import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import styled from 'styled-components';
import { styleVars } from '../styleVars';
import { ContainerMd, MainContainer } from '../styleClasses';

import LeftPanel from '../components/account-page/SidePanel'
import FormEditProfileContainer from '../components/account-page/FormEditProfileContainer'
import FormAddProductContainer from '../components/account-page/FormAddProductContainer'
import ListOfOwnedProducts from '../components/account-page/ListOfOwnedProducts'

import { connect } from "react-redux";
import { logOut } from '../actions/account';

const mapStateToProps = state => ({ account: state.account.account });
const mapDispatcheToProps = dispatch => ({ logOut: () => dispatch(logOut()) });

const Container = styled(ContainerMd)`
    @media all and (${styleVars.breakpoints.mediumUp}){
      display: grid;
      grid-column-gap: ${styleVars.spaces.spaceSm};
      grid-template-columns: repeat(2, 1fr)
    }
`;

const Account = (props) => {

  let account = props.account;

  return (
    <MainContainer>
      <Container>
        <LeftPanel account={account} logOut={props.logOut}/>
        <Switch>
          <Route
            path="/account/edit"
            component={ FormEditProfileContainer }
          />
          <Route
            path="/account/owned"
            component={ ListOfOwnedProducts }
          />
          <Route
            path="/account/new-product"
            component={ FormAddProductContainer }
          />
          <Redirect from="/account" to="/account/owned" />
        </Switch>
      </Container>
    </MainContainer>
  )
}

export default connect(mapStateToProps, mapDispatcheToProps)(Account)
