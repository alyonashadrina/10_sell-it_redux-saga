import React, { useEffect } from 'react';
import styled from 'styled-components';
import { MainContainer } from '../styleClasses';

import background from '../assets/bg-page-login.png';

import { connect } from "react-redux";
import { openDrawer, closeDrawer } from '../actions/layout';

const mapStateToProps = state => ({ drawerOpened: state.layout.drawerOpened });
const mapDispatcheToProps = dispatch => ({ openDrawer: () => dispatch(openDrawer()), closeDrawer: () => dispatch(closeDrawer()) });

const LoginContainer = styled(MainContainer)`
  background: url(${background});
  background-size: cover;
`;

const Login = (props) => {
    useEffect(() => {
      props.openDrawer();
      return () => {
        props.closeDrawer();
      };
    }, []);

    return (
      <LoginContainer />
    );
}

export default connect(mapStateToProps, mapDispatcheToProps)(Login)
