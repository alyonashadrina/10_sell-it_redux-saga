import React, { useEffect } from 'react';
import styled from 'styled-components';
import { styleVars } from '../styleVars';
import { ContainerMd, MainContainer } from '../styleClasses';

import Info from '../components/product-page/Info'

import { connect } from "react-redux";
import { fetchSingleProduct } from '../actions/products';

const mapStateToProps = state => ({ singleProduct: state.products.singleProduct });
const mapDispatcheToProps = dispatch => ({ fetchSingleProduct: (id) => dispatch(fetchSingleProduct(id)) });

const ProductContainer = styled(ContainerMd)`
    @media all and (${styleVars.breakpoints.mediumUp}){
      display: grid;
      grid-column-gap: ${styleVars.spaces.spaceSm};
      grid-template-columns: repeat(2, 1fr)
    }
`;

const ProductPage = (props) => {
  useEffect(() => {
    props.fetchSingleProduct(props.id)
  }, []);

  const product = props.singleProduct;
  let images = product.images;
  let image = 'https://pbs.twimg.com/profile_images/507251035929190400/BDUL3Uzt_400x400.png';
  if (images && images.length > 0) {
    image = images[0].file
  }

  return (
    <MainContainer>
      <ProductContainer>
        <img src={image} alt={ product.theme ? product.theme : ''}/>
        <Info product={product} />
      </ProductContainer>
    </MainContainer>
  );
}

export default connect(mapStateToProps, mapDispatcheToProps)(ProductPage)
