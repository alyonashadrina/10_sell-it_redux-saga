import React, { useEffect } from 'react';
import styled from 'styled-components';
import { styleVars } from '../styleVars';
import { ContainerMd, MainContainer } from '../styleClasses';

import Info from '../components/user-page/Info'

import { connect } from "react-redux";
import { fetchUser } from '../actions/account';

const mapStateToProps = state => ({ userToShow: state.account.userToShow });
const mapDispatcheToProps = dispatch => ({
  fetchUser: (name) => dispatch(fetchUser(name))
});


const UserContainer = styled(ContainerMd)`
    @media all and (${styleVars.breakpoints.mediumUp}){
      display: grid;
      grid-column-gap: ${styleVars.spaces.spaceSm};
      grid-template-columns: repeat(2, 1fr)
    }
`;

const UserPage = (props) => {
    useEffect(() => {
      props.fetchUser(props.name);
    }, []);

    let allUsers = props.userToShow;
    let user = {
      email: "",
      first_name: "",
      last_name: "",
      pk: 0,
      username: ""
    };
    if (allUsers.length > 0) {
      allUsers.map((obj, i) => {
        if (obj.username === props.name) {
          user = obj
          console.log('why there are no avatar and list of owned products?', obj)
        }
      });
    }

    return (
      <MainContainer>
        <UserContainer>
          <Info user={user}/>
        </UserContainer>
      </MainContainer>
    );
}

export default connect(mapStateToProps, mapDispatcheToProps)(UserPage)
