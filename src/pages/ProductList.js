import React, { useEffect } from 'react';
import { Link } from "react-router-dom";


import styled from 'styled-components';
import { styleVars } from '../styleVars';
import { ContainerLg, MainContainer } from '../styleClasses';

import ProductCard from '../components/product-card';

import { connect } from "react-redux";
import { fetchProducts } from '../actions/products';

const mapStateToProps = state => ({ productList: state.products.productList, searchWord: state.products.searchWord, totalPages: state.products.totalPages });
const mapDispatcheToProps = dispatch => ({ fetchProducts: (searchWord, page) => dispatch(fetchProducts(searchWord, page))  });

const ProductContainer = styled(ContainerLg)`
  display: grid;
  grid-column-gap: ${styleVars.spaces.spaceMd};
  grid-row-gap: ${styleVars.spaces.spaceMd};
  @media all and (${styleVars.breakpoints.smallUp}) {
    grid-template-columns: repeat(2, 1fr)
  }
  @media all and (${styleVars.breakpoints.mediumUp}) {
    grid-template-columns: repeat(4, 1fr)
  }
`;
const PaginationBtn = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid ${styleVars.colors.primary}
`;

const ProductList = (props) => {
  // TODO: fix searchWord request while being not on first page

  let page = window.location.href.indexOf("?page=") !== -1 ? window.location.href.slice(window.location.href.indexOf("?page=") + "?page=".length) : 1;
  page = parseInt(page)
  const prev = page - 1;
  const next = page + 1;

  useEffect(() => {
    props.fetchProducts(props.searchWord, page)
  },[props.searchWord]);

  let listOfProducts = props.productList.map((product, i) => {
    let text = ( product.theme ? product.theme : 'Item' )
    let image = ( product.images[0] ? product.images[0].file : 'https://pbs.twimg.com/profile_images/507251035929190400/BDUL3Uzt_400x400.png' )
    return <ProductCard img={image} title={text} id={product.pk} key={i}/>
  });

  let nextPage = () => {
    props.fetchProducts("", next)
  }

  let prevPage = () => {
    props.fetchProducts("", prev)
  }

  return (
    <MainContainer>
      <ProductContainer>
        {listOfProducts}
        { page !== 1 ? <Link to={`/?page=${prev}`} onClick={prevPage}> <PaginationBtn> Prev Page </PaginationBtn> </Link> : '' }
        { props.totalPages !== page ? <Link to={`/?page=${next}`} onClick={nextPage}> <PaginationBtn> Next Page </PaginationBtn> </Link> : ''}
      </ProductContainer>
    </MainContainer>
  );
}

export default connect(mapStateToProps, mapDispatcheToProps)(ProductList)
