import React from 'react';

export const ThemeContext = React.createContext(
  "transparent" // default value
);
