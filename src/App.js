import React from 'react';
import { Router } from "react-router-dom";
import { composeWithDevTools } from "redux-devtools-extension"
import reduxThunk from "redux-thunk";
import { applyMiddleware, createStore } from "redux";
import reducer from "./reducers";
import { Provider } from 'react-redux';
import logger from "./middlewars/logger"
import createSagaMiddleware from "redux-saga"
import rootSaga from "./saga/index"
import './App.css';

import { createBrowserHistory as createHistory } from 'history'
import httpService from './api-client/interceptors';

import Footer from './components/footer';
import Header from './components/header';
import MainContent from './components/MainContent';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer, composeWithDevTools(applyMiddleware(reduxThunk, logger, sagaMiddleware)))
sagaMiddleware.run(rootSaga);

const history = createHistory();
httpService.setupInterceptors(store, history);

const App = (props) => (
  <Provider store={store}>
    <Router history={history}>
        <div className="App">
          <Header />
          <MainContent/>
          <Footer />
        </div>
    </Router>
  </Provider>
)

export default App;
