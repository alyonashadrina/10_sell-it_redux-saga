import styled from 'styled-components';
import { styleVars } from './styleVars';

const Wrapper = styled.div`
  padding-right: ${styleVars.spaces.spaceSm};
  padding-left: ${styleVars.spaces.spaceSm};
  @media all and (${styleVars.breakpoints.mediumUp}){
    padding-right: ${styleVars.spaces.spaceLg};
    padding-left: ${styleVars.spaces.spaceLg};
  }
`;

const Button = styled.button`
  padding: ${styleVars.spaces.spaceXsm} ${styleVars.spaces.spaceSm};
  background: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg};
  font-size:${styleVars.fontSizes.small};
  cursor: pointer;
  text-align: center;
`;

const LabelButton = styled.label`
  padding: ${styleVars.spaces.spaceXsm} ${styleVars.spaces.spaceSm};
  background: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg};
  font-size:${styleVars.fontSizes.small};
  cursor: pointer;
  text-align: center;
`;

const Input = styled.input`
  border: 0;
  max-width: 100%;
  padding-top: calc(${styleVars.spaces.spaceXsm} / 2);
  padding-bottom: calc(${styleVars.spaces.spaceXsm} / 2);
  background: transparent;
  outline: none;
`;

const ContainerLg = styled(Wrapper)`
  max-width: 1440px;
  margin: auto;
`;

const ContainerMd = styled(Wrapper)`
  max-width: 950px;
  margin: auto;
`;

const MainContainer = styled.div`
  padding-top: calc(${styleVars.spaces.spaceXlg} * 2);
  padding-bottom: calc(${styleVars.spaces.spaceXlg} * 2);
  min-height: 100vh;
`;

export { Wrapper, Button, LabelButton, Input, ContainerLg, ContainerMd, MainContainer }
