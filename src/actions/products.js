import { FETCH_PRODUCTS_START,
         FETCH_SINGLE_PRODUCT_START,
         CHANGE_SEARCH_WORD,
         FETCH_OWNED_PRODUCTS_START,
         POST_PRODUCT_START,
         DELETE_PRODUCT_START,
         POST_PRODUCT_MESSAGE } from '../types/products';

const fetchProducts = (searchWord, page) => {
  return { type: FETCH_PRODUCTS_START, payload: { searchWord: searchWord, page: page } }
}

const fetchSingleProduct = (id) => {
  return { type: FETCH_SINGLE_PRODUCT_START, payload: id }
}

const changeSearchWord = (text) => dispatch => {
  dispatch({
      type: CHANGE_SEARCH_WORD,
      payload: text
  })
}

const fetchOwnedProducts = () => {
  return { type: FETCH_OWNED_PRODUCTS_START }
}


const addProduct = (data) => {
  return { type: POST_PRODUCT_START, payload: data }
}

const deleteProduct = (data) => {
  return { type: DELETE_PRODUCT_START, payload: data }
}

const setAddProductMessage = (text) => dispatch => {
  dispatch({
      type: POST_PRODUCT_MESSAGE,
      payload: text
  })
}


export { fetchProducts, fetchSingleProduct, changeSearchWord, fetchOwnedProducts, addProduct, deleteProduct, setAddProductMessage }
