import { CHANGE_LOG_MODE, TOGGLE_DRAWER } from '../types/layout';

const modeToLogin = () => dispatch => {
  dispatch({
      type: CHANGE_LOG_MODE,
      payload: 'login'
  })
}

const modeToRegister = () => dispatch => {
  dispatch({
      type: CHANGE_LOG_MODE,
      payload: 'register'
  })
}

const openDrawer = () => dispatch => {
  dispatch({
      type: TOGGLE_DRAWER,
      payload: true
  })
}

const closeDrawer = () => dispatch => {
  dispatch({
      type: TOGGLE_DRAWER,
      payload: false
  })
}

export { modeToLogin, modeToRegister, openDrawer, closeDrawer }
