import { LOG_IN_START,
         LOG_OUT_START,
         REGISTER_START,
         FETCH_PROFILE_START,
         EDIT_PROFILE_START,
         FETCH_USER_START } from '../types/account';

import { updateProfile } from "../api-client/account"


const logIn = (email, password) => {
  return { type: LOG_IN_START, payload: { email, password } }
}

const register = (username, email, password1, password2) => {
  return { type: REGISTER_START, payload: { username, email, password1, password2 } }
}

const logOut = () => {
  return { type: LOG_OUT_START }
}

const fetchProfile = () => {
  // console.log('FETCH_PROFILE_START')
  return { type: FETCH_PROFILE_START }
}

const editProfile = (data) => {
  // console.log('editProfile', data)
  return { type: EDIT_PROFILE_START, payload: data }
}

const fetchUser = (name) => {
  // console.log(`FETCH_USER_START`)
  return { type: FETCH_USER_START, payload: name }
}


export { logIn, logOut, register, fetchProfile, fetchUser, editProfile }
