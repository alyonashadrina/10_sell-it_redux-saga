import React from 'react';
import styled, { keyframes } from 'styled-components';

import { styleVars } from '../../styleVars';

const ldsDualRingSpin = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`
const LoaderContainer = styled.div`
  position: fixed;
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const LdsDualRing = styled.div`
  display: inline-block;
  width: 64px;
  height: 64px;
  &:after {
    content: " ";
    display: block;
    width: 46px;
    height: 46px;
    margin: 1px;
    border-radius: 50%;
    border: 5px solid ${styleVars.colors.primary};
    border-color:  ${styleVars.colors.primary} transparent  ${styleVars.colors.primary} transparent;
    animation: ${ldsDualRingSpin} 1s linear infinite;
  }
`;

const Preloader = () => (
  <LoaderContainer>
    <LdsDualRing />
  </LoaderContainer>
)

export default Preloader
