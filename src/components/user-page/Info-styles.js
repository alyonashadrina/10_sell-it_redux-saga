import styled from 'styled-components';
import { styleVars, pxToRem } from '../../styleVars';

export const InfoWrap = styled.div`
`;

export const Title = styled.div`
  display: grid;
  grid-template-columns: calc(${styleVars.spaces.spaceLg} * 3) 1fr;
  grid-column-gap: ${styleVars.spaces.spaceSm};
  align-items: center;
  padding-bottom: ${styleVars.spaces.spaceLg};

  h1 {
    font-size: ${pxToRem(32)};
    font-family: "Myriad Pro";
    color: rgb(0, 0, 0);
  }
`;

export const SellInfo = styled.div`
  font-weight: bold;
  padding-bottom: ${styleVars.spaces.spaceLg};
`;

export const Avatar = styled.img`
  width: calc(${styleVars.spaces.spaceLg} * 3);
  border-radius: 50px;
`;

export const Email = styled.span`
  padding-right: ${styleVars.spaces.spaceMd};
  a {
    color: ${styleVars.colors.primary}
  }
`;
