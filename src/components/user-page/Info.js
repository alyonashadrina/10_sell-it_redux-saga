import React from 'react';

import iconAvatar from '../../assets/avatar-black.png';
import { InfoWrap, Title, SellInfo, Email, Avatar } from './Info-styles'

const Info = ({ user }) => {
  return (
    <InfoWrap>
      <Title>
        <Avatar src={iconAvatar}/>
        <h1>{ ( user.first_name.length > 0 || user.last_name > 0 ) ? user.first_name + ' ' + user.last_name : user.username}</h1>
      </Title>
      <SellInfo>
        <Email>
          <a href={`mailto:${user.email}`}> {user.email}</a>
        </Email>
      </SellInfo>
    </InfoWrap>
  )
}

export default Info
