
const isEmail = string => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(string) ? true : false;
export const email = (value) =>
    value && !isEmail(value)
        ? 'Invalid email address'
        : undefined;

const isPassword = string => !( string.length < 8 || !(/\d/.test(string)) || !(/[a-z]/.test(string)) ) ? true : false;
export const password = (value) =>
    value && !isPassword(value)
        ? 'Must be 8 characters or more and contain numbers and characters'
        : undefined;
export const passwordRepeat = (value) =>
    value && !isPassword(value)
        ? 'Must be 8 characters or more and contain numbers and characters'
        : undefined;
// TODO: check if password is the same in both fields

const isName = string => !( string.length < 1 ) ? true : false;
export const name = (value) =>
    value && !isName(value)
        ? 'Must be 2 characters or more'
        : undefined;