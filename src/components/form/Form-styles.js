import styled, { css } from 'styled-components';
import { Button, Input } from '../../styleClasses';
import { styleVars } from '../../styleVars';

export const LabelText = styled.span`
  position: absolute;
  left: ${styleVars.spaces.spaceSm};
  top: 50%;
  transform: translateY(-50%);
  transition: transform .2s;
  text-align: left;
  display: inline-block;
  width: 100%;
`;

export const InputField = styled(Input)`
  width: 100%;
  padding-left: ${styleVars.spaces.spaceSm};
  color: ${styleVars.colors.text};
  ${props => (props.background === "primary") && css`
    color: ${styleVars.colors.onPrimaryBg};
  `}
  &:focus + ${LabelText},
  &:not(:placeholder-shown) + ${LabelText} {
    transform: translateY(${styleVars.fontSizes.base}) scale(.99);
  }
`;

export const SelectField = styled.select`
  width: 100%;
  padding-left: ${styleVars.spaces.spaceSm};
  color: ${styleVars.colors.text};
  ${props => (props.background === "primary") && css`
    color: ${styleVars.colors.onPrimaryBg};
    background: ${styleVars.colors.primary};
  `}
  & + ${LabelText} {
    top: unset;
    bottom: 0;
    transform: translateY(${styleVars.fontSizes.base}) scale(.9);
  }
`;

export const FieldGroup = styled.label`
  display: block;
  width: 100%;
  font-size: ${styleVars.fontSizes.small};
  position: relative;
  margin-bottom: ${styleVars.spaces.spaceXlg};
  position: relative;
  height: ${styleVars.fontSizes.large};
  color: ${styleVars.colors.text};
  ${props => (props.background === "primary") && css`
     color: ${styleVars.colors.onPrimaryBg};
  `}
`;

export const FieldGroupBordered = styled(FieldGroup)`
  border-bottom: 2px solid;
  height: unset;
  border-bottom-color: ${styleVars.colors.primary};
  color: ${styleVars.colors.text};
  ${props => (props.background === "primary") && css`
     border-bottom-color: ${styleVars.colors.onPrimaryBg};
     color: ${styleVars.colors.onPrimaryBg};
  `}
`;


export const TextareaField = styled.textarea`
  width: 100%;
  padding-left: ${styleVars.spaces.spaceSm};
  color: ${styleVars.colors.text};
  ${props => (props.background === "primary") && css`
     background: ${styleVars.colors.primary};
     color: ${styleVars.colors.onPrimaryBg};
  `}

  & + ${LabelText} {
    top: unset;
    bottom: 0;
  }
  &:focus + ${LabelText},
  &:not(:placeholder-shown) + ${LabelText} {
    transform: translateY(${styleVars.fontSizes.base}) scale(.9);

  }
`;

export const CheckboxField = styled.input`
  appearance: none;
  -moz-appearance: none;
  -webkit-appearance: none;
  z-index: -1;
  position: absolute;
  left: -10px;
  top: -8px;
  display: block;
  margin: 0;
  border-radius: 50%;
  width: 40px;
  height: 40px;
  background-color: rgba(var(--pure-material-onsurface-rgb, 0, 0, 0), 0.6);
  box-shadow: none;
  outline: none;
  opacity: 0;
  transform: scale(1);
  pointer-events: none;
  transition: opacity 0.3s, transform 0.2s;
  & + ${LabelText} {
    display: inline-block;
    width: 100%;
    cursor: pointer;
    &:before {
      content: "";
      display: inline-block;
      box-sizing: border-box;
      margin: 3px 11px 3px 1px;
      border: solid 2px; /* Safari */
      border-color: ${styleVars.colors.primary};
      ${props => (props.background === "primary") && css`
         border-color: ${styleVars.colors.onPrimaryBg};
      `}
      border-radius: 2px;
      width: 18px;
      height: 18px;
      vertical-align: middle;
      transition: border-color 0.2s, background-color 0.2s;
    }
    &:after {
      content: "";
      display: block;
      position: absolute;
      top: 3px;
      left: 1px;
      width: 10px;
      height: 5px;
      border: solid 2px transparent;
      border-right: none;
      border-top: none;
      transform: translate(3px, 4px) rotate(-45deg);
    }
  }
  &:checked + ${LabelText} {
    &:before {
      border-color: ${styleVars.colors.primary};
      background-color: ${styleVars.colors.primary};
      ${props => (props.background === "primary") && css`
        border-color: ${styleVars.colors.onPrimaryBg};
        background-color: ${styleVars.colors.onPrimaryBg};
      `}
    }
    &:after {
      border-color: ${styleVars.colors.onPrimaryBg};
      ${props => (props.background === "primary") && css`
        border-color: ${styleVars.colors.primary};
      `}
    }
  }
`;

export const Submit = styled(Button)`
  margin-top: ${styleVars.spaces.spaceLg};
  text-transform: capitalize;
  width: 100%;
  background: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg}
  ${props => (props.background === "primary") && css`
     background: ${styleVars.colors.onPrimaryBg};
     color: ${styleVars.colors.text}
  `}
  &:disabled {
    background: #eee;
    cursor: default;
  }
`;

export const Error = styled.span`
  color: ${styleVars.colors.error};
  font-size: ${styleVars.fontSizes.small};
`;

export const AddFiles = styled.label`
  padding: ${styleVars.spaces.spaceXsm} ${styleVars.spaces.spaceSm};
  background: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg};
  ${props => (props.background === "primary") && css`
    background: ${styleVars.colors.onPrimaryBg};
    color: ${styleVars.colors.text};
  `}
  font-size: ${styleVars.fontSizes.small};
  cursor: pointer;
  text-align: center;
  margin-bottom: ${styleVars.spaces.spaceLg};
  display: inline-block;
  input {
    width: 0.1px;
  	height: 0.1px;
  	opacity: 0;
  	overflow: hidden;
  	position: absolute;
  	z-index: -1;
  }
`;

export const FormProduct = styled.form`
  padding: ${styleVars.spaces.spaceMd} ${styleVars.spaces.spaceXsm};
`;

export const FormTitle = styled.div`
  font-weight: bold;
  cursor: pointer;
  margin-bottom: ${styleVars.spaces.spaceLg};
`;
