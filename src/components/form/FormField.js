import React from 'react'
import { LabelText, InputField, TextareaField, CheckboxField, SelectField, FieldGroup, AddFiles, FieldGroupBordered, Error } from './Form-styles'

import { ThemeContext } from '../../theme-context';

const renderField = ({ input, label, type, meta: { touched, error }, errorResponse, tag, children, value, theme, placeholder }) => {
  let field = <ThemeContext.Consumer>
      {(theme) => {
        return (
          <FieldGroupBordered background={theme}>
            <InputField {...input} type={type} placeholder={ placeholder ? placeholder : " "} background={theme}/>
            <LabelText > {label} {touched && ((error && <Error>{error}</Error>))} <Error>{errorResponse ? errorResponse : ""}</Error></ LabelText>
          </FieldGroupBordered>
        )
      }}
    </ThemeContext.Consumer>

  if (tag === "textarea") {
    field = <ThemeContext.Consumer>
        {(theme) => {
          return (<FieldGroupBordered>
              <TextareaField {...input} placeholder=" "/>
              <LabelText > {label} {touched && ((error && <Error>{error}</Error>))} <Error>{errorResponse ? errorResponse : ""}</Error></ LabelText>
            </FieldGroupBordered>
          )
        }}
      </ThemeContext.Consumer>
  }
  if (tag === "select") {
    field =  <ThemeContext.Consumer>
        {(theme) => {
          return (
            <FieldGroupBordered>
              <SelectField {...input}>
                {children}
              </SelectField>
              <LabelText > {label} {touched && ((error && <Error>{error}</Error>))} <Error>{errorResponse ? errorResponse : ""}</Error></ LabelText>
            </FieldGroupBordered>
          )
        }}
      </ThemeContext.Consumer>
  }
  if (type === "checkbox") {
    field =  <ThemeContext.Consumer>
        {(theme) => {
          return (
            <FieldGroup>
              <CheckboxField {...input} type={type} value="1"/>
              <LabelText > {label} {touched && ((error && <Error>{error}</Error>))} <Error>{errorResponse ? errorResponse : ""}</Error></ LabelText>
            </FieldGroup>
          )
        }}
      </ThemeContext.Consumer>
  }
  if (type === "file") {
    field =  <ThemeContext.Consumer>
        {(theme) => {
          return (
            <AddFiles>
              <InputField {...input} type={type} value={null} />
              {label} {touched && ((error && <Error>{error}</Error>))} <Error>{errorResponse ? errorResponse : ""}</Error>
            </AddFiles>
          )
        }}
      </ThemeContext.Consumer>
  }

  return field
}

export { renderField }
