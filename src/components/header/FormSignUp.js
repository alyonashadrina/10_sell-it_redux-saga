import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form'
import { compose } from "redux";
import { connect } from "react-redux";

import { Submit } from '../form/Form-styles'
import { renderField } from "../form/FormField";
import { email, password, name, passwordRepeat } from "../validators";
import { register } from '../../actions/account';

import { ThemeContext } from '../../theme-context';

const mapStateToProps = state => ({ loggedIn: state.account.loggedIn, registerErrors: state.account.registerErrors, stateForm: state.form });
const mapDispatcheToProps = dispatch => ({ register: ( username, email, password1, password2 ) => dispatch(register( username, email, password1, password2 )) });


class FormSignUp extends Component {
    syncCheck = (values) => {
      const { register, valid } = this.props;
      if (valid) {
        register( values.name, values.email, values.password, values.passwordRepeat )
      }
    };
    render() {

      const { registerErrors, pristine, submitting, handleSubmit } = this.props;

      return (
          <form onSubmit={handleSubmit(this.syncCheck)}>
              <Field name="name" type="text" label="Name"
                     component={renderField}
                     validate={name}
                     errorResponse={registerErrors.username ? registerErrors.username[0] : ''}/>
              <Field name="email" type="email" label="Email"
                     component={renderField}
                     validate={email}
                     errorResponse={registerErrors.email ? registerErrors.email[0] : ''}/>
              <Field name="password" type="password" label="Password"
                     component={renderField}
                     validate={password}
                     errorResponse={registerErrors.password1 ? registerErrors.password1 : ''}/>
              <Field name="passwordRepeat" type="password" label="Repeat password"
                     component={renderField}
                     validate={passwordRepeat}
                     errorResponse={registerErrors.password2 ? registerErrors.password2 : ''} />
              <Submit background={this.context} type="submit" disabled={pristine || submitting} > Submit </ Submit>
          </form>
      )
    }

}
FormSignUp.contextType = ThemeContext;

export default compose(
    connect(mapStateToProps, mapDispatcheToProps),
    reduxForm({
      form: 'FormSignUp',
    })
)(FormSignUp);
