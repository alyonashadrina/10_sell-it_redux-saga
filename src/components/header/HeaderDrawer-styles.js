import styled, { css } from 'styled-components';
import { styleVars } from '../../styleVars';
import { Wrapper, LabelButton } from '../../styleClasses';

export const Dropdown = styled(Wrapper)`
  max-width: 100%;
  position: fixed;
  width: ${styleVars.other.sidePanelWidth};
  height: 100vh;
  right: 0;
  background: ${styleVars.colors.primary};
  color:${styleVars.colors.onPrimaryBg};
  text-align: center;
  padding-bottom: ${styleVars.spaces.spaceSm};
  padding-top: calc((${styleVars.spaces.spaceSm} * 2 + ${styleVars.fontSizes.base} * ${styleVars.typography.lineHeight}) * 2);
  top: 0;
  transition: transform .2s;
  transform: translateY(-100%);
  ${props => props.opened && css`
    transform: translateY(0);
  `}
`;

export const LogMode = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: ${styleVars.spaces.spaceSm};
  padding-bottom: 5vh;
  padding-top: 3vh;

`;

export const ModeButton = styled(LabelButton)`
  position: relative;
  text-transform: capitalize;
  background: white;
  color: ${styleVars.colors.text};
  input {
    position: absolute;
    opacity: 0;
  }
  ${props => props.active && css`
    box-shadow: inset  0px -2px 0px 1px ${styleVars.colors.selected};
  `}
`;
