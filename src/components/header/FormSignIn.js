import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form'
import { compose } from "redux";
import { connect } from "react-redux";

import { Submit, Error } from '../form/Form-styles'
import { renderField } from "../form/FormField";
import { email, password } from "../validators";
import { logIn } from '../../actions/account';

import { ThemeContext } from '../../theme-context';

const mapStateToProps = state => ({
  loggedIn: state.account.loggedIn,
  loginErrors: state.account.loginErrors,
});
const mapDispatcheToProps = dispatch => ({
  logIn: (email, password) => dispatch(logIn(email, password))
});


class FormSignIn extends Component {
    syncCheck = (values) => {
      const { logIn, valid } = this.props;
      if (valid) {
        logIn(values.email, values.password )
      }
    };
    render() {

      const { loginErrors, pristine, submitting, handleSubmit } = this.props;

      return (
        <form onSubmit={handleSubmit(this.syncCheck)}>
          <Field
            name="email"
            type="email"
            label="Email"
            component={renderField}
            validate={email}
          />
          <Field
            name="password"
            type="password"
            label="Password"
            component={renderField}
            validate={password}
          />
          <Error>{loginErrors}</Error>
          <Submit
            background={this.context}
            type="submit"
            disabled={ pristine || submitting } >
            Submit
          </Submit>
        </form>
      )
    }
}

FormSignIn.contextType = ThemeContext;

export default compose(
    connect(mapStateToProps, mapDispatcheToProps),
    reduxForm({
      form: 'FormSignIn',
    })
)(FormSignIn);