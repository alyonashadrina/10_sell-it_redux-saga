import React, { useEffect } from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

import { HeaderBlock, HeaderLogo } from './Header-styles';

import { connect } from "react-redux";

import HeaderLogged from './HeaderLogged';
import HeaderNotLogged from './HeaderNotLogged';
import HeaderSearch from './HeaderSearch';

import logoBlueSm from '../../assets/logo-blue--sm.png';

import { fetchProfile } from '../../actions/account';

const mapStateToProps = state => ({ loggedIn: state.account.loggedIn });
const mapDispatcheToProps = dispatch => ({ fetchProfile: () => dispatch(fetchProfile()) });

const Header = ({ fetchProfile, loggedIn }) => {
  useEffect(() => {
    fetchProfile()
  }, []);

  return (
    <HeaderBlock>
      <HeaderLogo>
        <Link to="/">
          <img src={logoBlueSm} alt="Sell it! - logo"/>
        </Link>
      </HeaderLogo>
      <HeaderSearch />
      { loggedIn ? <HeaderLogged/> : <HeaderNotLogged /> }
    </HeaderBlock>
  )
}

export default connect(mapStateToProps, mapDispatcheToProps)(Header)
