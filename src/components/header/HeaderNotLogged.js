import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import { AccountNotLogged, AccountWrapper, AccountProfile, AccountIcon, LinkSpan } from './HeaderNotLogged-styles';

import iconAccount from '../../assets/icon-account.svg';
import HeaderDrawer from './HeaderDrawer';

import { connect } from "react-redux";
import { modeToLogin, modeToRegister, openDrawer, closeDrawer } from '../../actions/layout';

const mapStateToProps = state => ({ logMode: state.layout.logMode, drawerOpened: state.layout.drawerOpened });
const mapDispatcheToProps = dispatch => ({
  modeToRegister: () => dispatch(modeToRegister()),
  modeToLogin: () => dispatch(modeToLogin()),
  openDrawer: () => dispatch(openDrawer()),
  closeDrawer: () => dispatch(closeDrawer()),
 });

 const ModeButtons = ({ modeToLogin, modeToRegister }) => {
   return (
     <>
       Welcome,
       <LinkSpan onClick={ modeToLogin }> login </LinkSpan>
       or
       <LinkSpan onClick={ modeToRegister }> register </LinkSpan>
       for start!
     </>
   )
 }

 const ModeLinks = ({ modeToLogin, modeToRegister }) => {
   return (
     <>
       Welcome,
       <Link to="/login/sign-in">
         <LinkSpan onClick={ modeToLogin }> login </LinkSpan>
       </Link>
       or
       <Link to="/login/sign-up">
         <LinkSpan onClick={ modeToRegister }> register </LinkSpan>
       </Link>
       for start!
     </>
   )
 }

 const HeaderNotLogged = ({ modeToLogin, modeToRegister, openDrawer, closeDrawer, drawerOpened }) => {
   const _modeToLogin = () => {
     modeToLogin()
     openDrawer()
   }

   const _modeToRegister = () => {
     modeToRegister()
     openDrawer()
   }

   const _toggleDrawer = () => {
     if(drawerOpened) {
       closeDrawer()
     } else {
       openDrawer()
     }
   }

   return (
     <AccountNotLogged>
       <AccountWrapper>
         <AccountProfile>
           <Switch>
             <Route
               path="/login"
               render={props => <ModeLinks {...props} modeToLogin={_modeToLogin} modeToRegister={_modeToRegister} />}
             />
             <Route
               path="/"
               render={props => <ModeButtons {...props} modeToLogin={_modeToLogin} modeToRegister={_modeToRegister} />}
             />
           </Switch>
         </AccountProfile>
         <AccountIcon onClick={_toggleDrawer}>
           <img src={iconAccount} alt="open login panel"/>
         </AccountIcon>
       </AccountWrapper>
       <HeaderDrawer opened={drawerOpened}/>
     </AccountNotLogged>
   )
 }

export default connect(mapStateToProps, mapDispatcheToProps)(HeaderNotLogged)
