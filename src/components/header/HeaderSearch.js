import React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";
import styled from 'styled-components';
import { styleVars } from '../../styleVars';
import { Wrapper, Input } from '../../styleClasses';

import { connect } from "react-redux";
import { changeSearchWord } from '../../actions/products';

import iconSearch from '../../assets/icon-search.svg';

const mapStateToProps = state => ({ searchWord: state.products.searchWord });
const mapDispatcheToProps = dispatch => ({ changeSearchWord: (text) => dispatch(changeSearchWord(text)) });

const Search = styled(Wrapper)`
  padding-top: ${styleVars.spaces.spaceSm};
  padding-bottom: ${styleVars.spaces.spaceSm};
  align-self: center;
  form {
    position: relative;
  }
  a {
    position: absolute;
    left: ${styleVars.spaces.spaceXsm} / 2;
    bottom: ${styleVars.spaces.spaceXsm};
  }
`;

const SearchInput = styled(Input)`
  border-bottom: 2px solid ${styleVars.colors.primary};
  width: 100%;
  max-width: 450px;
  padding-left: ${styleVars.spaces.spaceLg};
  font-size: pxToRem(14);
`;

const HeaderSearch = ({ searchWord, changeSearchWord }) => {
  const _handleSearch = (event) => {
    changeSearchWord(event.target.value);
  }

  return (
    <Search>
      <form>
        <Link to={`/search/${searchWord}`}>
          <button type="submit">
            <img src={iconSearch} alt="search" />
          </button>
        </Link>
        <SearchInput type="text" onChange={_handleSearch}/>
      </form>
    </Search>
  )
}

export default connect(mapStateToProps, mapDispatcheToProps)(HeaderSearch)
