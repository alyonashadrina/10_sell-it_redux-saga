import styled from 'styled-components';
import { styleVars } from '../../styleVars';
import { Wrapper, Button } from '../../styleClasses';

export const AccountDropdown = styled.div`
  display: none;
  grid-template-columns: 1fr 1fr;
  grid-column-start: 1;
  grid-column-end: 3;
  font-weight: normal;
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;

  a:hover:before {
    content: '';
    display: block;
    position: absolute;
    width: 5px;
    height: 100%;
    left: 0;
    top: 0;
    background: ${styleVars.colors.text};
  }
`;

export const AccountDropdownBtn = styled(Button)`
  position: relative;
  text-align: center;
  &:hover:before {
    content: '';
    display: block;
    position: absolute;
    width: 5px;
    height: 100%;
    left: 0;
    top: 0;
    background: ${styleVars.colors.text};
  }
`;

export const AccountLogged = styled.div`
  padding-top: ${styleVars.spaces.spaceXsm};
  padding-bottom: ${styleVars.spaces.spaceXsm};
  background-color: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg};
  position: relative;
  display: grid;
  align-items: center;
  a {
    color: ${styleVars.colors.onPrimaryBg};
  }
  &:hover ${AccountDropdown} {
    @media all and (${styleVars.breakpoints.mediumUp}){
      display: grid;
    }
  }
`;

export const LogOut = styled.span``

export const AccountWrapper = styled(Wrapper)`
    display: grid;
    align-items: center;
    font-weight: bold;
    ${LogOut} {
      display: none;
    }
    @media all and (${styleVars.breakpoints.mediumUp}){
      grid-template-columns: 1fr 25px;
      ${LogOut} {
        display: inline;
      }
    }

`;

export const AccountProfile = styled.span`
    img {
      border-radius: 100%;
      width: 40px;
      margin-right: ${styleVars.spaces.spaceXsm};
      vertical-align: middle;
    }
    span {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
      max-width: 240px;
      display: inline-block;
      vertical-align: text-top;
      display: none;
      @media all and (${styleVars.breakpoints.mediumUp}){
        display: inline;
      }
    }
`;
