import React, { useEffect } from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

import { AccountDropdown, AccountDropdownBtn, AccountLogged, AccountWrapper, AccountProfile, LogOut } from './HeaderLogged-styles';

import iconAccount from '../../assets/icon-account.svg';
import iconAvatar from '../../assets/avatar.png';

import { connect } from "react-redux";
import { logOut, fetchProfile } from '../../actions/account';

const mapStateToProps = state => ({ account: state.account.account });
const mapDispatcheToProps = dispatch => ({ logOut: () => dispatch(logOut()), fetchProfile: () => dispatch(fetchProfile()) });

const HeaderLogged = ({ fetchProfile, account, logOut }) => {
  useEffect(() => {
    fetchProfile()
  }, []);

  return (
    <AccountLogged>
      <AccountWrapper>
        <AccountProfile>
          <Link to="/account/edit">
            <img src={account.avatar ? account.avatar : iconAvatar } alt={account.username + ' avatar'}/>
            <span>{account.username}</span>
          </Link>
        </AccountProfile>
        <LogOut onClick={logOut}>
          <Link to="/login">
            <img src={iconAccount} alt="log out"/>
          </Link>
        </LogOut>
      </AccountWrapper>
      <AccountDropdown>
        <AccountDropdownBtn href="#"> <Link to="/account/new-product"> Add new post </Link></AccountDropdownBtn>
        <AccountDropdownBtn href="#"> <Link to="/account"> btn Profile  </Link></AccountDropdownBtn>
      </AccountDropdown>
    </AccountLogged>
  )
}

export default connect(mapStateToProps, mapDispatcheToProps)(HeaderLogged)
