import styled from 'styled-components';
import { styleVars } from '../../styleVars';
import { Wrapper } from '../../styleClasses';

export const HeaderBlock = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  z-index: 1;
  display: grid;
  height: 57px;
  color: ${styleVars.colors.primary};
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-columns: 20% 1fr calc(25px + ${styleVars.spaces.spaceSm} * 2);
  @media all and (${styleVars.breakpoints.mediumUp}){
    grid-template-columns: 195px 1fr ${styleVars.other.sidePanelWidth};
  }
`;

export const HeaderLogo = styled(Wrapper)`
  padding: 5px;
  align-self: center;
`;
