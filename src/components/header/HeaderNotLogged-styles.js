import styled from 'styled-components';
import { styleVars } from '../../styleVars';
import { Wrapper } from '../../styleClasses';

export const AccountNotLogged = styled.div`
  padding-top: ${styleVars.spaces.spaceXsm};
  padding-bottom: ${styleVars.spaces.spaceXsm};
  background-color: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg};
  position: relative;
  display: grid;
  align-items: center;
  position: relative;
  a {
    color: ${styleVars.colors.onPrimaryBg};
  }
  a  {
    color: ${styleVars.colors.text};
  }

`;

export const AccountWrapper = styled(Wrapper)`
  display: grid;
  align-items: center;
  font-weight: bold;
  z-index: 1;
  @media all and (${styleVars.breakpoints.mediumUp}){
    grid-template-columns: 1fr 25px;
  }
`;

export const AccountProfile = styled.p`
  display: none;
  @media all and (${styleVars.breakpoints.mediumUp}){
    display: block;
  }
`;

export const AccountIcon = styled.a`
  display: block;
  @media all and (${styleVars.breakpoints.mediumUp}){
    display: none;
  }
`;
export const LinkSpan = styled.span`
  cursor: poiner;
  color: ${styleVars.colors.text}
`;
