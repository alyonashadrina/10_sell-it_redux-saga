import React from 'react';
import { Switch, Route, Redirect, Link } from 'react-router-dom';

import { Dropdown, LogMode, ModeButton } from './HeaderDrawer-styles';

import logoWhiteMd from '../../assets/logo-white--md.png';
import FormSignIn  from './FormSignIn';
import FormSignUp  from './FormSignUp';

import { connect } from "react-redux";
import { modeToLogin, modeToRegister } from '../../actions/layout';

import { ThemeContext } from '../../theme-context';

const mapStateToProps = state => ({ logMode: state.layout.logMode });
const mapDispatcheToProps = dispatch => ({ modeToRegister: () => dispatch(modeToRegister()), modeToLogin: () => dispatch(modeToLogin()) });

const ModeButtons = (props) => {
  return (
    <LogMode>
      <ModeButton active={ props.logMode === 'login' ? true : false } onClick={props.modeToLogin}>
        <input type="radio" name="mode" value="Sign In" />Sign In
      </ModeButton>
      <ModeButton active={ props.logMode === 'login' ? false : true } onClick={props.modeToRegister}>
        <input type="radio" name="mode" value="Sign Up"/>Sign Up
      </ModeButton>
    </LogMode>
  )
}

const ModeLinks = (props) => {
  return (
    <LogMode>
      <ModeButton active={ props.logMode === 'login' ? true : false }  onClick={props.modeToLogin}>
        <Link to="/login/sign-in">
          <input type="radio" name="mode" value="Sign In" />Sign In
        </Link>
      </ModeButton>
      <ModeButton active={ props.logMode === 'login' ? false : true } onClick={props.modeToRegister}>
        <Link to="/login/sign-up">
          <input type="radio" name="mode" value="Sign Up"/>Sign Up
        </Link>
      </ModeButton>
    </LogMode>
  )
}

const HeaderDrawer = (props) => {
  let elProps = props;
  return (
    <ThemeContext.Provider value="primary">
    <Dropdown opened={props.opened}>
      <img className="side-panel__logo" src={logoWhiteMd} alt="Sell it! - logo"/>

      <Switch>
        <Route
          path="/login"
          render={props => <ModeLinks {...props} logMode={elProps.logMode} modeToLogin={elProps.modeToLogin} modeToRegister={elProps.modeToRegister} />}
        />
        <Route
          path="/"
          render={props => <ModeButtons {...props} logMode={elProps.logMode} modeToLogin={elProps.modeToLogin} modeToRegister={elProps.modeToRegister} />}
        />
      </Switch>

      <Switch>
        <Route
          path="/login/sign-in"
          component={ FormSignIn }
        />
        <Route
          path="/login/sign-up"
          component={ FormSignUp }
        />
        <Route exact path="/login" render={() => <Redirect to="/login/sign-in" />} />
        <Route
          path="/"
          component={ props.logMode === 'login' ? FormSignIn : FormSignUp }
        />
      </Switch>

    </Dropdown>
    </ThemeContext.Provider>
  )
}
export default connect(mapStateToProps, mapDispatcheToProps)(HeaderDrawer)
