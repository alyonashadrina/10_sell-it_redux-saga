import React from "react";

import { shallow } from "enzyme";

import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import renderer from "react-test-renderer";

import Footer from "./index"

Enzyme.configure({ adapter: new Adapter() });

describe('<Footer />', () => {
  it("render component with one p", () => {
    const component = shallow(<Footer />);
    expect(component.find("p")).toHaveLength(1);
  });

  it("renders correctly", () => {
    const component = shallow(<Footer />);
    expect(component).toMatchSnapshot();
  });
})
