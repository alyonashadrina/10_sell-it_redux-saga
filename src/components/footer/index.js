import React from 'react';
import styled from 'styled-components';
import { styleVars } from '../../styleVars';

const FooterBlock = styled.footer`
  background-color: ${styleVars.colors.primary};
  padding: ${styleVars.spaces.spaceSm} ${styleVars.spaces.spaceLg};
  color: ${styleVars.colors.onPrimaryBg};
  text-align: center;
  font-weight: bold;
  position: absolute;
  bottom: 0;
  width: 100%;
`;

const Footer = () => (
  <FooterBlock>
    <p>
      2017 - front-end labs Light IT
    </p>
  </FooterBlock>
)

export default Footer;
