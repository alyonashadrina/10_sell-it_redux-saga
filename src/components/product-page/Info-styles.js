import styled from 'styled-components';
import { styleVars, pxToRem } from '../../styleVars';

export const InfoWrap = styled.div`
`;

export const Title = styled.div`
  h1 {
    font-size: ${pxToRem(32)};
    font-family: "Myriad Pro";
    color: rgb(0, 0, 0);
    padding-bottom: ${styleVars.spaces.spaceLg};
  }
`;

export const SellInfo = styled.div`
  padding-bottom: ${styleVars.spaces.spaceLg};
`;

export const Seller = styled.span`
  font-weight: bold;
  padding-right: ${styleVars.spaces.spaceMd};
  a {
    color: ${styleVars.colors.primary}
  }
`;

export const Price = styled.span`
  font-weight: bold;
  color: ${styleVars.colors.selected};
  font-size: ${pxToRem(32)};
  display: inline-block;
  &:after {
    content: ' $'
  }
`;

export const Description = styled.div`
  padding-top: ${styleVars.spaces.spaceMd};
`;
