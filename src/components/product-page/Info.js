import React from 'react';
import { Link } from "react-router-dom";
import { InfoWrap, Title, SellInfo, Seller, Price, Description } from './Info-styles'

const Info = ({ product = {}}) => {

  let owner = product.owner;
  let ownerName = '';
  let userName = '';

  if (owner) {
   ownerName = owner.first_name + ' ' + owner.last_name;
   userName = owner.username;
  }

  return (
    <InfoWrap>
      <Title>
        { product.category ? <p> {product.category} </p> : ''}
        <h1>{ product.theme }</h1>
      </Title>
      <SellInfo>
        <Seller>
          from <Link to={`/users/${userName}`}> { ownerName.length > 1 ? ownerName : userName} </Link>
        </Seller>
        <Price>{product.price}</Price>
        { product.contract_price ? <p> (contract price)</p> : '' }
        { product.text ? <Description> {product.text} </Description> : ''  }
      </SellInfo>
    </InfoWrap>
  )



}

export default Info
