import React from 'react';
import { Route, NavLink  } from 'react-router-dom';

import { styleVars } from '../../styleVars';
import iconAvatar from '../../assets/avatar-black.png';
import { SidePanel, Info, Title, Avatar, UserInfo, Email, Navigation } from './SidePanel-styles'

let LeftPanel = ({account, logOut}) => {
  return (
    <SidePanel>
      <Info>
        <Title>
          <Avatar src={ account.avatar ? account.avatar : iconAvatar } alt="ownerName"/>
          <div>
            <h1>
              { ( account.first_name && account.last_name ) ? account.first_name + ' ' +  account.last_name : account.username }
            </h1>
            <p> { account.location } </p>
          </div>
        </Title>
        <UserInfo>
          <Email>
          <a href={ `mailto:${account.email}` }> { account.email }</a>
          </Email>
        </UserInfo>
      </Info>
      <Navigation>
        <NavLink to="/account/edit" activeStyle={{ background: styleVars.colors.primary, color: styleVars.colors.onPrimaryBg }}>
          Edit profile
        </NavLink>
        <NavLink to="/account/owned" activeStyle={{ background: styleVars.colors.primary, color: styleVars.colors.onPrimaryBg }}>
          My products
        </NavLink>
        <NavLink to="/account/new-product" activeStyle={{ background: styleVars.colors.primary, color: styleVars.colors.onPrimaryBg }}>
          Add new product
        </NavLink>
        <NavLink to="/login" activeStyle={{ background: styleVars.colors.primary, color: styleVars.colors.onPrimaryBg }} onClick={logOut}>
          Log out
        </NavLink>
      </Navigation>
    </SidePanel>
  )
}

export default LeftPanel
