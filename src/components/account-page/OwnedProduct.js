import React, { useEffect } from 'react';

import { Container, DeleteButton, EditButton, ProductInfo } from './OwnedProduct-styles'

import { connect } from "react-redux";
import { deleteProduct, fetchOwnedProducts } from '../../actions/products';

const mapDispatcheToProps = dispatch => ({ deleteProduct: (id) => dispatch(deleteProduct(id)), fetchOwnedProducts: () => dispatch(fetchOwnedProducts()) });

const OwnedProduct = ({ product, deleteProduct, fetchOwnedProducts }) => {
  const _deleteProduct = () => {
    console.log(`delete ${product.pk}`)
    deleteProduct(product.pk)
    fetchOwnedProducts();
  }
  const _editProduct = () => {
    console.log(`edit ${this.props.product.pk}`)
  }

  return (
    <Container>
      <img src='https://pbs.twimg.com/profile_images/507251035929190400/BDUL3Uzt_400x400.png' alt={product.theme}/>
        <ProductInfo>
          <p>{product.theme}</p>
          <span>{product.currency} {product.price}</span>
        </ProductInfo>
      <EditButton onClick={_editProduct} title={`Edit ${product.theme}`}></EditButton>
      <DeleteButton onClick={_deleteProduct} title={`Delete ${product.theme}`}></DeleteButton>
    </Container>
  )
}

export default connect(null, mapDispatcheToProps)(OwnedProduct)
