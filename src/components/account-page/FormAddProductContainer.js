import React from 'react';
import { connect } from 'react-redux';
import FormAddProduct from './FormAddProduct';
import { addProduct, setAddProductMessage } from '../../actions/products';

const mapDispatcheToProps = dispatch => ({ addProduct: (data) => dispatch(addProduct(data)) });

let FormAddProductContainer = ({ addProduct, values }) => (
  <FormAddProduct
    onSubmit={values => {
      // console.log(values)
      addProduct(values)
    }}
  />
)

export default connect(null, mapDispatcheToProps)(FormAddProductContainer)
