export const validate = values => {
    const errors = {};
    if (!values.theme) {
        errors.theme = 'Required'
    }

    return errors;
}
