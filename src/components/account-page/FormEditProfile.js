import React from 'react';
import { reduxForm, Field } from 'redux-form'

import { Submit, AddFiles, FormProduct, FormTitle } from '../form/Form-styles'
import { renderField } from "../form/FormField";

// import { validate } from "./FormSign-validation";

let FormEditProfile = ({ handleChange, handleSubmit, value, initialValues }) => {
    console.log('initialValues', initialValues)
    return (
      <FormProduct onSubmit={handleSubmit}>
        <FormTitle>Edit profile</FormTitle>
        <Field name="username" type="text" label="Username" component={renderField} />
        <Field name="first_name" type="text" label="First name" component={renderField} />
        <Field name="last_name" type="text" label="Last name" component={renderField} />
        <AddFiles>
          <input type="file" name="images" id="addImages" placeholder="images" accept="image/png,image/gif,image/jpeg,image/jpg" />
          Add avatar
        </AddFiles>
        <Field name="location" type="text" label="Location" component={renderField} />
        <Field name="color_scheme" type="text" label="Color scheme" component={renderField} />
        <Field name="language" type="text" label="Language" component={renderField} tag="select" options={['EN', 'RU', 'UA']}/>
        <Submit type="submit">Update profile</Submit>
      </FormProduct>
    )
  }

FormEditProfile = reduxForm({
  form: 'FormAddProduct',
  // validate
})(FormEditProfile);
export default FormEditProfile
