import styled from 'styled-components';

import { styleVars, pxToRem } from '../../styleVars';

export const SidePanel = styled.div`
`;

export const Info = styled.div`
`;

export const Navigation = styled.nav`
  a {
    display: block;
    padding: ${styleVars.spaces.spaceSm};
    &:hover {
      background: ${styleVars.colors.border};
    }
  }

`;


export const Title = styled.div`
  display: grid;
  grid-template-columns: calc(${styleVars.spaces.spaceLg} * 3) 1fr;
  grid-column-gap: ${styleVars.spaces.spaceSm};
  align-items: center;
  padding-bottom: ${styleVars.spaces.spaceLg};

  h1 {
    /* display: inline-block; */
    font-size: ${pxToRem(32)};
    font-family: "Myriad Pro";
    color: rgb(0, 0, 0);

  }
`;

export const UserInfo = styled.div`
  font-weight: bold;
  padding-bottom: ${styleVars.spaces.spaceLg};
`;

export const Email = styled.span`
  padding-right: ${styleVars.spaces.spaceMd};
  a {
    color: ${styleVars.colors.primary}
  }
`;

export const Avatar = styled.img`
  width: calc(${styleVars.spaces.spaceLg} * 3);
  border-radius: 50px;
`;
