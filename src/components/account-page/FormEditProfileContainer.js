import React from 'react';
import { connect } from 'react-redux';
import FormEditProfile from './FormEditProfile';
import { editProfile } from '../../actions/account';

const mapStateToProps = state => ({ account: state.account.account });
const mapDispatcheToProps = dispatch => ({ editProfile: (data) => dispatch(editProfile(data)) });

let FormAddProductContainer = ({ account, editProfile, values }) => (
  <FormEditProfile
    onSubmit={values => {
      editProfile(values)
    }}
    initialValues={account}
  />
)

export default connect(mapStateToProps, mapDispatcheToProps)(FormAddProductContainer)
