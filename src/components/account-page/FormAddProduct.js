import React, { useEffect } from 'react';
import { reduxForm, Field } from 'redux-form'

import { Submit, Error, FormProduct, FormTitle } from '../form/Form-styles'
import { renderField } from "../form/FormField";
import { validate } from "./FormAddProduct-validation";

import { connect } from "react-redux";
import { setAddProductMessage } from '../../actions/products';

const mapStateToProps = state => ({ postProductMesage: state.products.postProductMesage });
const mapDispatcheToProps = dispatch => ({ setAddProductMessage: (text) => dispatch(setAddProductMessage(text)) });


let FormAddProduct = ({ handleChange, handleSubmit, value, postProductMesage, setAddProductMessage }) => {
    useEffect(() => {
      setAddProductMessage('')
    }, []);
    return (
      <FormProduct onSubmit={handleSubmit}>
        <FormTitle>Add new product</FormTitle>
        <Field name="theme" type="text" label="Theme" component={renderField} />
        <Field name="category" type="text" label="Category" component={renderField} />

        <Field name="images" type="file" label="Add images" component={renderField} />

        <Field name="is_active" value="is_active" type="checkbox" label="Is active" component={renderField} />
        <Field name="location" type="text" label="Location" component={renderField} />
        <Field name="price" type="number" label="Price" component={renderField} />
        <Field name="currency" label="Currency" component={renderField} tag="select" >
          { ['USD', 'RUB', 'UAH'].map((option, i) => <option value={option} key={i}>{option}</option>) }
        </Field>
        <Field name="contract_price" type="checkbox" label="Contract price" component={renderField} />
        <Field name="text" label="Description" component={renderField} tag="textarea"/>
        { postProductMesage }
        <Submit type="submit">add product</Submit>
      </FormProduct>
    )
  }

FormAddProduct = reduxForm({
  form: 'FormAddProduct',
  validate
})(FormAddProduct);

export default connect(mapStateToProps, mapDispatcheToProps)(FormAddProduct)
