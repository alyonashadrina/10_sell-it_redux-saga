import React, { useEffect } from 'react';

import { connect } from "react-redux";
import { fetchOwnedProducts } from '../../actions/products';

import OwnedProduct from './OwnedProduct'

const mapStateToProps = state => ({ ownedProducts: state.products.ownedProducts });
const mapDispatcheToProps = dispatch => ({ fetchOwnedProducts: () => dispatch(fetchOwnedProducts()) });

const ListOfOwnedProducts = ({ fetchOwnedProducts, ownedProducts }) => {
  useEffect(() => {
    fetchOwnedProducts()
  }, []);

  return (
    <div>
      List of owned products
      { ownedProducts.length > 0 ? ownedProducts.map( product => <OwnedProduct product={product}/> ) : <p> No products yet </p>}
    </div>
  )
}

export default connect(mapStateToProps, mapDispatcheToProps)(ListOfOwnedProducts)
