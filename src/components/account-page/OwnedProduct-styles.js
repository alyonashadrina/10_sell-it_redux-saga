import styled from 'styled-components';
import { styleVars } from '../../styleVars';

import deleteIcon from '../../assets/icon-delete-24px.svg'
import editIcon from '../../assets/icon-edit-24px.svg'

export const Container = styled.div`
    /* @media all and (${styleVars.breakpoints.mediumUp}){ */
      display: grid;
      grid-column-gap: ${styleVars.spaces.spaceXsm};
      grid-template-columns: .25fr .55fr .1fr .1fr;
    /* } */
`;

export const DeleteButton = styled.button`
  background-image: url(${deleteIcon});
  background-repeat: no-repeat;
  background-position: center;
  cursor: pointer;
`;

export const EditButton = styled.button`
  background-image: url(${editIcon});
  background-repeat: no-repeat;
  background-position: center;
  cursor: pointer;
`;

export const ProductInfo = styled.div`
  align-self: center;
`;
