import React, { Suspense } from 'react';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import { connect } from "react-redux";
import { closeDrawer } from '../actions/layout';

import Preloader from './loader/Preloader'

const ProductList = React.lazy(() => import('../pages/ProductList'));
const ProductPage = React.lazy(() => import('../pages/ProductPage'));
const UserPage = React.lazy(() => import('../pages/UserPage'));
const Login = React.lazy(() => import('../pages/Login'));
const Account = React.lazy(() => import('../pages/AccountPage'));

const mapStateToProps = state => ({ drawerOpened: state.layout.drawerOpened });
const mapDispatcheToProps = dispatch => ({
  closeDrawer: () => dispatch(closeDrawer()),
 });

const MainContent = (props) => (
  <div onClick={props.closeDrawer}>
    <Suspense fallback={<div><Preloader/></div>}>
      <Route
        exact path="/"
        render={props =>
          <ProductList {...props} />
        }
      />
      <Route
        path="/?page=:num"
        render={({ match }) => (
          <ProductList page={match.params.num} />
        )}
      />
      <Route
        path="/search/:key"
        render={({ match }) => (
          <ProductList urlSearchWord={match.params.key} />
        )}
      />
      <Route
        exact path="/search"
        render={() => <Redirect to="/"/>}
      />

      <Route
        path="/login"
        render={props => <Login {...props} />}
      />
      <Route
        path="/account"
        render={props => <Account {...props} />}
      />

      <Route
        path="/products/:productId"
        render={({ match }) => (
          <ProductPage id={match.params.productId} />
        )} />

      <Route path="/users/:user"
        render={({ match }) => (
          <UserPage name={match.params.user} />
        )} />
    </Suspense>
  </div>
)

export default connect(mapStateToProps, mapDispatcheToProps)(MainContent)
