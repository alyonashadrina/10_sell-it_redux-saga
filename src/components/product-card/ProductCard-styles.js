import styled from 'styled-components';
import { styleVars } from '../../styleVars';

import eye from '../../assets/icon-eye.svg';

export const CardTitle = styled.p`
  background: ${styleVars.colors.primary};
  color: ${styleVars.colors.onPrimaryBg};
  padding: ${styleVars.spaces.spaceXsm} ${styleVars.spaces.spaceXsm} ${styleVars.spaces.spaceXsm} ${styleVars.spaces.spaceXsm};
  position: relative;
  &:after {
    content: ' ';
    display: block;
    width: 20px;
    height: 20px;
    background: url(${eye});
    background-size: contain;
    background-repeat: no-repeat;
    position: absolute;
    right: ${styleVars.spaces.spaceXsm};
    top: 50%;
    transform: translateY(-50%);
  }
  span {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    display: inline-block;
    vertical-align: text-top
    max-width: calc((100vw - (${styleVars.spaces.spaceSm} * 2)) - 50px);
    @media all and (${styleVars.breakpoints.smallUp}) {
      max-width: calc(((100vw - (${styleVars.spaces.spaceSm} * 2 + ${styleVars.spaces.spaceMd})) / 2) - 50px);
    }

    @media all and (${styleVars.breakpoints.mediumUp}){
      max-width: calc(((100vw - (${styleVars.spaces.spaceLg} * 2 + ${styleVars.spaces.spaceMd} * 3)) / 4) - 70px);
    }
  }
`;

export const ImgContainer = styled.div`

  /* width: calc(100vw - (${styleVars.spaces.spaceSm} * 2)); */
  height: calc(100vw - (${styleVars.spaces.spaceSm} * 2));
  img {
    object-fit: none;
    height: 100%;
    width: 100%;
  }

  @media all and (${styleVars.breakpoints.smallUp}) {
    height: calc((100vw - (${styleVars.spaces.spaceSm} * 2 + ${styleVars.spaces.spaceMd})) / 2);
  }

  @media all and (${styleVars.breakpoints.mediumUp}){
    height: calc((100vw - (${styleVars.spaces.spaceLg} * 2 + ${styleVars.spaces.spaceMd} * 3)) / 4);
  }
`;
