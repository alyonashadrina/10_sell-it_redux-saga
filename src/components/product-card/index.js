import React from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

import { CardTitle, ImgContainer } from './ProductCard-styles';

const ProductCard = (props) => (
  <div className="product-card">
    <Link to={`/products/${props.id}`}>
      <ImgContainer>
        <img src={props.img} alt={props.title} />
      </ImgContainer>
      <CardTitle>
        <span>
          {props.title}
        </span>
      </CardTitle>
    </Link>
  </div>
)

export default ProductCard
