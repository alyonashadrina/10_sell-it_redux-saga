const logger = store => next => action => {
  if (action.meta && action.meta.printLog) {
    console.log('dispatching', action)
  }
  let result = next(action)
  if (action.meta && action.meta.printLog) {
    console.log('next state', store.getState())
  }
  return result
}

export default logger
