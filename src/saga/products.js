import { call, put, all, takeLatest } from "redux-saga/effects"
import { fetchAllProducts, fetchProduct, fetchOwned, addProduct, delProduct } from "../api-client/products"

import { FETCH_PRODUCTS_START,
         FETCH_SINGLE_PRODUCT_START,
         FETCH_PRODUCTS,
         FETCH_SINGLE_PRODUCT,
         POST_PRODUCT_START,
         FETCH_OWNED_PRODUCTS_START,
         FETCH_OWNED_PRODUCTS,
         DELETE_PRODUCT_START,
         POST_PRODUCT_MESSAGE } from '../types/products';

function* fetchProducts(action) {
  try {
    const response = yield call(fetchAllProducts, action.payload);
    // console.log('fetchProducts',action,response)
    yield put({ type: FETCH_PRODUCTS, payload: response.data})
  } catch (error) {
    console.log('fetchProducts error', error)
    yield put({ type: FETCH_PRODUCTS, payload: []})
  }
}

function* fetchSingleProduct(action) {
  try {
    const response = yield call(fetchProduct, action.payload);
    yield put({ type: FETCH_SINGLE_PRODUCT, payload: response.data})
  } catch (error) {
    console.log('fetchSingleProduct error', error)
    yield put({ type: FETCH_SINGLE_PRODUCT, payload: {}})
  }
}

function* postProduct(action) {
  try {
    const response = yield call(addProduct, action.payload);
    console.log('saga postProduct', response)
    yield put({ type: POST_PRODUCT_MESSAGE, payload: response.statusText})
  } catch (error) {
    console.log('saga postProduct error', error.response)
    yield put({ type: POST_PRODUCT_MESSAGE, payload: 'Theme is required'})
  }
}

function* fetchOwnedProducts(action) {
  try {
    const response = yield call(fetchOwned);
    // console.log('saga fetchOwnedProducts', response)
    yield put({ type: FETCH_OWNED_PRODUCTS, payload: response.data.data})
  } catch (error) {
    console.log('saga fetchOwnedProducts error', error.response)
  }
}

function* deleteProduct(action) {
  try {
    const response = yield call(delProduct, action.payload);
    // console.log('saga deleteProduct', response)
  } catch (error) {
    console.log('saga deleteProduct error', error.response)
  }
}

function* productsSaga() {
    yield all([
        yield takeLatest(FETCH_PRODUCTS_START, fetchProducts),
        yield takeLatest(FETCH_SINGLE_PRODUCT_START, fetchSingleProduct),
        yield takeLatest(POST_PRODUCT_START, postProduct),
        yield takeLatest(FETCH_OWNED_PRODUCTS_START, fetchOwnedProducts),
        yield takeLatest(DELETE_PRODUCT_START, deleteProduct),
    ]);
}

export default productsSaga
