import { all, spawn } from "redux-saga/effects"
import productsSaga from "./products"
import accountSaga from "./account"

export default function* rootSaga() {
  yield all([
    spawn(accountSaga),
    spawn(productsSaga),
  ])
}