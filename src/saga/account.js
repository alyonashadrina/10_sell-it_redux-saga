import { takeEvery, call, put, all, takeLatest } from "redux-saga/effects"
import { requestLogin, requestLogout, requestRegister, requestAccount, requestUser, updateProfile } from "../api-client/account"

import { LOG_IN_START,
         LOG_IN,
         SET_LOGIN_ERRORS,
         LOG_OUT_START,
         LOG_OUT,
         REGISTER_START,
         SET_REGISTER_ERRORS,
         FETCH_PROFILE_START,
         FETCH_PROFILE,
         EDIT_PROFILE_START,
         FETCH_USER_START,
         FETCH_USER } from '../types/account';

function* logIn(action) {
  try {
    const response = yield call(requestLogin, action.payload);
    localStorage.setItem("user-sell-it", response.data.token );
    yield put({ type: LOG_IN, payload: response.data});
    yield put({ type: SET_LOGIN_ERRORS, payload: []});
  } catch (error) {
    yield put({ type: SET_LOGIN_ERRORS, payload: error.response.data.non_field_errors});
  }
}

function* register(action) {
  try {
    const response = yield call(requestRegister, action.payload);
    localStorage.setItem("user-sell-it", response.data.token );
    yield put({ type: LOG_IN, payload: response.data})
    yield put({ type: SET_REGISTER_ERRORS, payload: {}});
  } catch (error) {
    yield put({ type: SET_REGISTER_ERRORS, payload: error.response.data});
  }
}

function* logOut() {
  try {
    yield call(requestLogout);
    localStorage.removeItem("user-sell-it");
    yield put({ type: LOG_OUT })
  } catch (error) {
    localStorage.removeItem("user-sell-it");
    yield put({ type: LOG_OUT })
  }
}

function* editProfile(action) {
  try {
    console.log('editProfile to send', JSON.stringify(action.payload))
    const response = yield call(updateProfile, JSON.stringify(action.payload));
    console.log('saga updateProfile', response)
  } catch (error) {
    console.log('saga updateProfile error', error.response)
  }
}

function* getAccount() {
  try {
    const response = yield call(requestAccount);
    // console.log('getAccount response.data', response.data)
    yield put({ type: FETCH_PROFILE, payload: response.data})
  } catch (error) {
    localStorage.removeItem("user-sell-it");
    yield put({ type: LOG_OUT_START });
  }
}

function* getUser(action) {
  try {
    const response = yield call(requestUser, action.payload);
    // console.log('getUser response.data', response.data)
    yield put({ type: FETCH_USER, payload: response.data})
  } catch (error) {
    console.log('requestUser:', error)
  }
}

function* accountSaga() {
    yield all([
        yield takeLatest(LOG_IN_START, logIn),
        yield takeLatest(REGISTER_START, register),
        yield takeLatest(LOG_OUT_START, logOut),
        yield takeLatest(EDIT_PROFILE_START, editProfile),
        yield takeLatest(FETCH_PROFILE_START, getAccount),
        yield takeLatest(FETCH_USER_START, getUser),
    ]);
}

export default accountSaga
