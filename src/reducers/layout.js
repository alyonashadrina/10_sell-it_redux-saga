import { CHANGE_LOG_MODE, TOGGLE_DRAWER } from '../types/layout';

const initialState = {
  logMode: 'login',
  drawerOpened: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_LOG_MODE:
        return { ...state, logMode: action.payload }
    case TOGGLE_DRAWER:
        return { ...state, drawerOpened: action.payload }
    default:
      return state;
  }
};
