import { LOG_IN,
         LOG_OUT,
         SET_LOGIN_ERRORS,
         SET_REGISTER_ERRORS,
         FETCH_PROFILE,
         FETCH_USER } from '../types/account';

const initialState = {
  loggedIn: false,
  token: null,
  account: {
    id: null,
    username: null,
    email: null,
    first_name: null,
    last_name: null,
    avatar: null,
    location: null,
    color_scheme: null,
    language: null
  },
  loginErrors: [],
  registerErrors: {},
  userToShow: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN:
        return { ...state,
                 loggedIn: true,
                 token: action.payload.token,
                 account: action.payload.user
               }
    case LOG_OUT:
        return { ...state,
                loggedIn: false,
                token: null,
                // account: null
              }
    case FETCH_PROFILE:
        return { ...state,
                 loggedIn: true,
                 account: action.payload
               }
    case SET_LOGIN_ERRORS:
        return { ...state,
                 loginErrors: action.payload
               }
    case SET_REGISTER_ERRORS:
        return { ...state,
                 registerErrors: action.payload
               }
     case FETCH_USER:
         return { ...state,
                  userToShow: action.payload
                }

    default:
      return state;
  }
};
