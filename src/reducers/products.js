import { FETCH_PRODUCTS,
         FETCH_SINGLE_PRODUCT,
         CHANGE_SEARCH_WORD,
         FETCH_OWNED_PRODUCTS,
         POST_PRODUCT_MESSAGE } from '../types/products';

const initialState = {
  productList: [],
  totalPages: 1,
  singleProduct: {},
  searchWord: window.location.href.indexOf("search/") !== -1 ? window.location.href.slice(window.location.href.indexOf("search/") + "search/".length) : '',
  ownedProducts: [],
  postProductMesage: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS:
        return { ...state, productList: action.payload.data, totalPages: action.payload.meta.total }
    case FETCH_SINGLE_PRODUCT:
      return { ...state, singleProduct: action.payload }
    case CHANGE_SEARCH_WORD:
      return { ...state, searchWord: action.payload }
    case FETCH_OWNED_PRODUCTS:
      return { ...state, ownedProducts: action.payload }
    case POST_PRODUCT_MESSAGE:
      return { ...state, postProductMesage: action.payload }
    default:
      return state;
  }
};
