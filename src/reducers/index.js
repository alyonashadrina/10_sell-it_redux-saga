import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'


import products from './products';
import layout from './layout';
import account from './account';

export default combineReducers({
  products,
  layout,
  account,
  form: formReducer,
})
