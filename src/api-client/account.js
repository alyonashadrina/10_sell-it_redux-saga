import { get, post, put, patch } from "./base";

export const requestLogin = (data) => {
  return (post('login/', data))
}

export const requestRegister = (data) => {
  return ( post('registration/', data) )
}

export const requestAccount = () => {
  return ( get('profile/') )
}

export const requestUser = (name) => {
  return ( get('users/?'+name) )
}

export const updateProfile = (data) => {
  console.log('api updateProfile', data)
  return ( patch('profile/', data) )
}

export const requestLogout = () => post('logout/')
