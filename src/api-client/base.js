import axios from "axios";

const request = ( method, path, data={}, headers={} ) => {
  const token = localStorage.getItem("user-sell-it");

  if(token) {
    headers['Authorization'] = `JWT ${token}`
  }

  return (
    axios({
      method: method,
      url: 'http://light-it-04.tk/api/' + path,
      data: data,
      headers: headers
    })
  )
}

export const get = ( path, headers ) => request( 'get', path, '', headers )

export const post = ( path, data, headers ) => request( 'post', path, data, headers )

export const del = ( path ) => request( 'delete', path )

export const put = ( path ) => request( 'put', path )

export const patch = ( path ) => request( 'patch', path )
