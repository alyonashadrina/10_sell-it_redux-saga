import { get, post, del } from "./base";

export const fetchAllProducts = ( { searchWord, page } ) => get(`posters/?page=${page}&search=${searchWord}`)
export const fetchProduct = (id) => get('posters/' + id + '/?format=json')
export const fetchOwned = () => get('posters/own/')
export const addProduct = (data) => post('posters/', data)
export const delProduct = (id) => del('posters/' + id + '/' )
