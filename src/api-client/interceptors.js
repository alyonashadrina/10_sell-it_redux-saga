import axios from "axios"
// import { logOut } from "../actions/account"

export default {
  setupInterceptors: (store, history) => {

      axios.interceptors.response.use(response => {
        if ( response.data.token ) {
          history.push('/');
        }
        return response;
      }, error => {

      if (error.response.status === 401 && ( history.location.pathname !== '/' && history.location.pathname !== '/login/sign-up' && !history.location.pathname.includes('products') ) ) {
        history.push('/login');
      }

      if (error.response.status === 404) {
         history.push('/not-found');
      }

      return Promise.reject(error);
    });
  },
};
