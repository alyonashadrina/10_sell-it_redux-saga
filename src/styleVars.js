let pxToRem = (size) => ( `${(size /18).toFixed(2)}rem` );

let colors = {
  blue: '#00b3be',
  white: '#fff',
  grey: '#4a4a4a',
  green: '#91d763',
  red: 'red',
  lightgrey: 'lightgrey'
};

let varibles = {
  colors: {
    primary: colors.blue,
    onPrimaryBg: colors.white,
    text: colors.grey,
    selected: colors.green,
    error: colors.red,
    border: colors.lightgrey,
  },
  spaces: {
    spaceXsm: pxToRem(10),
    spaceSm: pxToRem(15),
    spaceMd: pxToRem(20),
    spaceLg: pxToRem(25),
    spaceXlg: pxToRem(50)
  },
  breakpoints: {
    smallUp: 'min-width: 600px',
    mediumUp: 'min-width: 1024px',
    largeUp: 'min-width: 1440px',
    xlargeUp: 'min-width: 1920px',
  },
  fontSizes: {
    base: '18px',
    small: pxToRem(14),
    xsmall: pxToRem(12),
    large: pxToRem(24)
  },
  typography: {
    lineHeight: 1.2
  },
  other: {
    sidePanelWidth: '380px'
  }
}



export { varibles as styleVars, pxToRem }
